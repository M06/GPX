import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.basex.BaseXServer;
import org.basex.api.client.ClientSession;
import org.basex.core.Context;
import org.basex.core.cmd.XQuery;

/**
 * @author iam47594953
 *
 */

public class BasexServer implements Runnable {

	private boolean stop = false;
	
	@Override
	public void run() {
		synchronized (this) {
			BaseXServer server = null;
			
			try {
				server = new BaseXServer();
				while (!stop) {
					System.out.println("SERVER:[" + System.currentTimeMillis()
							+ "] Server BASE X running... ");
					this.wait(60000);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					server.stop();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}		
			
			
		}
	}

	
	
	

}
