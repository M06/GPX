import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.cs.Db4oClientServer;

public class DB4ODAO implements Runnable {

	ObjectContainer db4oClient = null;
	private boolean connect = false;

	/**
	 * al ejecutarse el metodo start() la clase dao se contecta al servidor
	 */
	@Override
	public void run() {
		try {
			db4oClient = Db4oClientServer.openClient("localhost", 8733,
					"user1", "1234");// podemos usar localhost o 127.0.0.1
			this.connect = true;
			while (true) {

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db4oClient.close();
		}

	}

	/**
	 * almazenamos la accion realizada por el usuario
	 * 
	 * @param useraccion
	 *            accion realizada por el usuario
	 */
	public void storeAccion(UserAccion useraccion) {
		db4oClient.store(useraccion);
		db4oClient.commit();
	}

	/**
	 * recuperamos todas las acciones realizadas por todos los usuarios y las imprimimos por pantalla
	 */	
	public void recuperarAcciones() {
		UserAccion e = new UserAccion(null, null, true);
		ObjectSet<UserAccion> result = db4oClient.queryByExample(e);
		listaResultados(result);
	}

	/**
	 * imprimimos por pantall una seria de objetos
	 * 
	 * @param result
	 */
	public static void listaResultados(ObjectSet<UserAccion> result) {
		System.out.println("Número resultados: " + result.size());
		while (result.hasNext()) {
			UserAccion e = result.next();
			System.out.println(e.toString());
		}
	}

	/**
	 * devuelve si la case clase dao se a conectado al servidor
	 * 
	 * @return
	 */
	public boolean isConnect() {
		return connect;
	}
}
