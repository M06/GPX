import com.db4o.ObjectServer;
import com.db4o.cs.Db4oClientServer;

/**
 * @author iam47594953
 *
 */
public class DB4OServer implements Runnable {
	
	private boolean running = false;	
	private boolean stop = false;

	public void run() {
		synchronized (this) {
			ObjectServer server = Db4oClientServer.openServer(
					Db4oClientServer.newServerConfiguration(), "Nserver.yap",
					8733);

			Thread.currentThread().setName(this.getClass().getName());

			server.grantAccess("user1", "1234");
			try {
				running = true;
				while (!stop) {
					System.out.println("SERVER:[" + System.currentTimeMillis()
							+ "] Server DB4O running... ");
					this.wait(60000);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				server.close();
			}
		}
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
	
	

}
