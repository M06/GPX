
public class UserAccion {

	private Usuario usuario;
	private String accion;
	private boolean successful;
	
	public UserAccion(Usuario usuario, String accion, boolean successful) {
		super();
		this.usuario = usuario;
		this.accion = accion;
		this.successful = successful;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}
	
		
	
}
