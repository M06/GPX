import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.basex.api.client.ClientSession;
import org.basex.core.cmd.Open;
import org.basex.util.list.IntList;

public class BasexDAO implements Runnable {

	private static ClientSession session;
	private boolean connect = false;
	
	public BasexDAO() {
	}
	
	@Override
	public void run() {
		try {
			session = new ClientSession("localhost", 1984, "admin", "admin");
			session.execute(new Open("database"));
			connect = true;
			// session.execute(new CreateDB("database"));
			while (true) {

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param f fichero de resumenes que añadiremos a basex
	 */
	public void addResumen(File f,String fileName) {		
		try {			
			Path path = Paths.get(f.getPath());
			InputStream is = new ByteArrayInputStream(Files.readAllBytes(path));
			session.add(fileName,is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * añadimos un archivo a basex
	 * 
	 * @param f
	 *            archivo a guardar
	 */
	public void addFile(File f,String fileName) {
		Path path = Paths.get(f.getPath());

		try {
			InputStream is = new ByteArrayInputStream(Files.readAllBytes(path));
			session.store(fileName, is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void recuperaFichero(String file){
		// XML documents
	   // final IntList il = data.resources.docs();
	}

	public boolean isConnect() {
		return connect;
	}

	public void setConnect(boolean connect) {
		this.connect = connect;
	}

	
	
}
