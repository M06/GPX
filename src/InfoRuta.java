import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class InfoRuta {

	private File file;

	public InfoRuta(File file) {
		this.file = file;
	}

	public String[] datos(String nameFileResumenXML) {
		String[] datos = new String[6];

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		Document doc;
		try {
			doc = dbFactory.newDocumentBuilder().parse(file);
			// formatea el texto (si hay saltos de linea raros y demás)
			doc.getDocumentElement().normalize();

			double desPositivo = 0;
			NodeList eles = doc.getElementsByTagName("ele");
			for (int i = 0; i < eles.getLength() - 1; i++) {
				double eleActual = Double.parseDouble(eles.item(i)
						.getTextContent());
				double eleSiguiente = Double.parseDouble(eles.item(i + 1)
						.getTextContent());
				double desnivel = eleSiguiente - eleActual;
				// miremos en que variable va
				if (desnivel > 0) {
					desPositivo = desPositivo + desnivel;
				}
			}
			datos[3] = desPositivo+"";
			
			NodeList nombre = doc.getElementsByTagName("name");			
			String nombreRuta = nombre.item(0).getTextContent();
			datos[0] = nombreRuta;
			NodeList desc = doc.getElementsByTagName("desc");
			String descripcion = desc.item(0).getTextContent();
			datos[2] = descripcion;
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			// Añadimos elemento raiz
			Document docResum = docBuilder.newDocument();
			Element rootElement = docResum.createElement("resumen");
			docResum.appendChild(rootElement);

			// recorremos la lista

			Element ruta = docResum.createElement("ruta");
			rootElement.appendChild(ruta);

			Attr attr = docResum.createAttribute("nombre");
			attr.setValue(nombreRuta);
			ruta.setAttributeNode(attr);

			Element descInsert = docResum.createElement("descripcion");
			descInsert.appendChild(docResum.createTextNode(descripcion));
			ruta.appendChild(descInsert);

			Element desP = docResum.createElement("desnivel_positivo");
			desP.appendChild(docResum.createTextNode(desPositivo + ""));
			ruta.appendChild(desP);

			

			// escribimos el contenido en un fichero
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			// XML indentado
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(docResum);
			StreamResult result = new StreamResult(new File(nameFileResumenXML));
			transformer.transform(source, result);

		} catch (SAXException | IOException | ParserConfigurationException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//devolvemos la informacion del fixero gpx
		return datos;
	}
}
