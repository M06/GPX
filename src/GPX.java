import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GPX {

//	private static ObjectContainer db4oClient = null;
//	static DB4ODAO dB4ODAO;
	
	public static void main(String[] args) {

		// creem els servidors basex i db4o amb les seves clases dao
		// indicant un temps de 3 segons perque doni temps de iniciarse el servidor
		//abans no es conectela clase dao
		ScheduledExecutorService ses = Executors.newScheduledThreadPool(2);
		ses.schedule(new Thread(new BasexServer()), 0, TimeUnit.SECONDS);
		BasexDAO basexDAO = new BasexDAO();
		
//		ses.schedule(new Thread(basexDAO), 3, TimeUnit.SECONDS);		
//		DB4OServer dB4OServer = new DB4OServer();		
		
//		ScheduledExecutorService ses2 = Executors.newScheduledThreadPool(2);
//		ses2.schedule(new Thread(dB4OServer), 0, TimeUnit.SECONDS);		
		
//		ses2.schedule(new Thread(dB4ODAO), 3, TimeUnit.SECONDS);
//		DB4OServer dB4OServer = new DB4OServer();
//		dB4OServer.run();
//		while(!dB4OServer.isRunning()){
//			
//		}
		
//		dB4ODAO = new DB4ODAO();
//		dB4ODAO.run();
		
//		DB4ODAO dB4ODAO = new DB4ODAO();
//		if(dB4OServer.isRunning()){
			
//		}
		

		// creem una instancia de la clase que gistionara les transaccions amb
		// postgres
		PostgresDAO postgresServerDAO = new PostgresDAO();
		// la iniciem perque es conecti a la base de dades
		postgresServerDAO.start();


		
		// añadiendo las rutas de los ficheros en cada arrayList
		ArrayList<String> rutas = new ArrayList<String>();
		ArrayList<String> images = new ArrayList<String>();
		ArrayList<String> videos = new ArrayList<String>();

		for (int i = 0; i < args.length; i++) {
			String text = args[i];
			if (text.equals("-ruta")) {
				Boolean stop = false;
				for (int k = ++i; k < args.length && !stop; k++) {

					if (args[k].contains("-")) {
						stop = true;
					} else {
						rutas.add(args[k]);
					}
				}
			} else if (text.equals("-image")) {
				Boolean stop = false;
				for (int k = ++i; k < args.length && !stop; k++) {

					if (args[k].contains("-")) {
						stop = true;
					} else {
						images.add(args[k]);
					}
				}
			} else if (text.equals("-video")) {
				Boolean stop = false;
				for (int k = ++i; k < args.length && !stop; k++) {

					if (args[k].contains("-")) {
						stop = true;
					} else {
						videos.add(args[k]);
					}
				}
			}
		}
		
		// obtenemos el primer argumento de la comanda
		String comand = args[0];
		// añadir usuario
		switch (comand) {
		case "-add":
			Usuario usuario = new Usuario(args[1], args[2]);
			try {
				// añadimos el usuario a la base de datos
				postgresServerDAO.addUser(usuario);
			} catch (ExceptionShortPass e) {
				System.out
						.println("ERROR: contrasenya de usuario demasiado corta, minimo 6 caracteres");
				// si salta algun error lo guardamos en db4o
//				dB4ODAO
//						.storeAccion(new UserAccion(usuario, "login", false));
			} catch (ExceptionUserExist e) {
				System.out.println("ERROR: El usuario ya existe");
//				dB4ODAO.storeAccion(new UserAccion(usuario, "-add", false));
			} catch (Exception e) {
//				dB4ODAO.storeAccion(new UserAccion(usuario, "-add", false));
			}
			// si la accion se a realizado correctamente lo gurdamosn en db4o
//			dB4ODAO.storeAccion(new UserAccion(usuario, "-add", true));
			break;

		// comprobamos el usuario y la contraseña
		case "-user":
			Usuario usuarioAcces = new Usuario(args[1], args[3]);
			if (!postgresServerDAO.checkUserAcces(usuarioAcces)) {
				System.out.println("Usuario o contraseña incorrecta");
//				dB4ODAO.storeAccion(new UserAccion(usuarioAcces, "login",
//						false));
			} else {
//				dB4ODAO.storeAccion(new UserAccion(usuarioAcces, "login",
//						true));
				if (args[4].equals("-ruta")) {
					int newIDRuta = 0;
					try {
						for (int i = 0; i < rutas.size(); i++) {
							// recuperem el fitxer gpx
							File fgpx = new File(rutas.get(i));
							// obtenim el que sera el nou id de la nova ruta
							// sabem que sera aquest perque el que fem es contar
							// el
							// numero de rutas i sumar li u,aquest sera el no id
							newIDRuta = postgresServerDAO.getNewIdForRuta();
							// el mateix que abans
							int newIDDocument = postgresServerDAO
									.getNewIdForDocument();
							// generem el nom que tindra el fitxer de resum xml
							String nameFileResumenxml = args[1] + newIDRuta
									+ newIDDocument;
							// obtenim el id de l'usuari que inserta la ruta
							int idUser = postgresServerDAO.getUserID(args[1]);

							InfoRuta infoRuta = new InfoRuta(fgpx);
							// obtenim tota la informacio de les rutes
							// i li pasem el nom que tindras que tindre el
							// fitxer
							String[] datos = infoRuta.datos(nameFileResumenxml);
							postgresServerDAO.insertRuta(datos[0], datos[2],
									Double.parseDouble(datos[3]),
									nameFileResumenxml, idUser);

							// insertamos la informacion del documento
							postgresServerDAO.insertDocument(
									nameFileResumenxml, newIDRuta);

							// añadimos el reument en el fichero xml
							// baseXDAO.addResumen();
							// basexServer.addResumen(new
							// File(nameFileResumenxml));
						}
						for (int e = 0; e < images.size(); e++) {
							// recuperem el fitxer gpx
							File fgpx = new File(images.get(e));

							// el mateix que abans
							int newIDDocument = postgresServerDAO
									.getNewIdForDocument();
							// generem el nom que tindra el fitxer de resum xmls
							String nameFileResumenxml = args[1] + newIDRuta
									+ newIDDocument;
							// insertamos la informacion del documento
							postgresServerDAO.insertDocument(
									nameFileResumenxml, newIDRuta);
							// añadimos el reument en el fichero xml
							// baseXDAO.addResumen();
//							basexDAO.addFile(fgpx, nameFileResumenxml);
						}
						for (int e = 0; e < videos.size(); e++) {
							// recuperem el fitxer gpx
							File fgpx = new File(videos.get(e));

							// el mateix que abans
							int newIDDocument = postgresServerDAO
									.getNewIdForDocument();
							// generem el nom que tindra el fitxer de resum xmls
							String nameFileResumenxml = args[1] + newIDRuta
									+ newIDDocument;
							// insertamos la informacion del documento
							postgresServerDAO.insertDocument(
									nameFileResumenxml, newIDRuta);
							// añadimos el reument en el fichero xml
							// baseXDAO.addResumen();
//							basexDAO.addFile(fgpx, nameFileResumenxml);
						}
					} catch (Exception e) {
						// si la accion no se a realizado correctamente lo gurdamosn en db4o
//						dB4ODAO.storeAccion(new UserAccion(usuarioAcces,
//								"-ruta", false));
					}
//					dB4ODAO.storeAccion(new UserAccion(usuarioAcces,
//							"-ruta", true));
					// si la accion se a realizado correctamente lo gurdamosn en db4o

				} else if (args[4].equals("-output")) {
					try {
						if (!postgresServerDAO.checkUserAcces(usuarioAcces)) {
							System.out
									.println("Usuario o contraseña incorrecta");
						} else {
							// ArrayList<File> resumenes =
							// PostgresDAO.selectAllRutasFromUsuario(user);
						}
					} catch (Exception e) {
						// si la accion no se a realizado correctamente lo gurdamosn en db4o
//						dB4ODAO.storeAccion(new UserAccion(usuarioAcces,
//								"-output", false));
					}
					// si la accion se a realizado correctamente lo gurdamosn en db4o
//					dB4ODAO.storeAccion(new UserAccion(usuarioAcces,
//							"-output", true));
				} else if (args[1].equals("-find")) {
					try {
						if (!postgresServerDAO.checkUserAcces(usuarioAcces)) {
							System.out
									.println("Usuario o contraseña incorrecta");
						} else {
							// ArrayList<File> resumenes =
							// PostgresDAO.selectAllRutasFromUsuario(user);
						}
					} catch (Exception e) {
						// si la accion no se a realizado correctamente lo gurdamosn en db4o
//						dB4ODAO.storeAccion(new UserAccion(usuarioAcces,
//								"-find", false));
					}
					// si la accion se a realizado correctamente lo gurdamosn en db4o
//					dB4ODAO.storeAccion(new UserAccion(usuarioAcces,
//							"-find", true));
				} else if (args[1].equals("-output")) {
					try {

					} catch (Exception e) {
						// si la accion no se a realizado correctamente lo gurdamosn en db4o
//						dB4ODAO.storeAccion(new UserAccion(usuarioAcces,
//								"-output all rutas", false));
					}
					// si la accion se a realizado correctamente lo gurdamosn en db4o
//					dB4ODAO.storeAccion(new UserAccion(usuarioAcces,
//							"-output all rutas", true));
				}
//				dB4ODAO.recuperarAcciones();
			}
			break;

		default:
			break;
		}
		System.exit(0);
	}	

	
	
}
