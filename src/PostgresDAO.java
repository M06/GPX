import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PostgresDAO {

	private Connection con;
	private Statement st;
	private ResultSet rs;

	public PostgresDAO() {

	}

	/**
	 * conectem la clase dao amb la base de dades
	 */
	public void start() {

		try {
			// Cargamos driver
			Class.forName("org.postgresql.Driver");
			System.out.println(" Driver caragdo ");

			con = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/gpx", "test", "123456");

			st = con.createStatement();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * donat un usuari comprobem la seva contrasenya
	 * 
	 * @param user
	 * @return
	 */
	public boolean checkUserAcces(Usuario user) {
		Boolean correctUserPass = false;
		String name = user.getName();
		String pass = user.getPass();

		String query = "select nombre from usuarios where pass = ?";
		try {
			PreparedStatement Pst = con.prepareStatement(query);
			Pst.setString(1, pass);
			ResultSet result = Pst.executeQuery();
			if (result.next()) {
				correctUserPass = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return correctUserPass;
	}

	/**
	 * añade el usuario en el caso de que el usuario no exista y el password sea
	 * correcto
	 */
	public void addUser(Usuario user) throws ExceptionShortPass,
			ExceptionUserExist {

		String userName = user.getName();
		String userPass = user.getPass();

		if (userPass.length() < 6) {
			throw new ExceptionShortPass();
		} else if (checkUser(userName)) {
			throw new ExceptionUserExist();
		} else {
			String query = "insert into usuarios (nombre, pass) values(?,?)";
			try {
				PreparedStatement pSt = con.prepareStatement(query);
				pSt.setString(1, userName);
				pSt.setString(2, userPass);
				pSt.execute();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

	/**
	 * comprueba si el usuario ya existe
	 */
	public boolean checkUser(String nombre) {
		Boolean exist = false;
		String query = "select nombre from usuarios where nombre = ?";
		try {
			PreparedStatement pSt = con.prepareStatement(query);
			pSt.setString(1, nombre);
			rs = pSt.executeQuery();
			exist = rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return exist;
	}

	/**
	 * recuperamos el id de un usuario dado
	 * 
	 * @param nombre nombre del usurio
	 * @return el id del usurio pasado por parametro
	 */
	public int getUserID(String nombre) {
		int userID = 0;
		String query = "select id from usuarios where nombre = ?";
		try {
			PreparedStatement pSt = con.prepareStatement(query);
			pSt.setString(1, nombre);
			rs = pSt.executeQuery();
			rs.next();
			userID = rs.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userID;
	}

	/**
	 * comprueba que el pass sea valido
	 */
	public boolean checkPass(String pass) {
		return pass.length() >= 6;
	}

	/**
	 * 
	 * @return el ultim id de la taula rutas
	 */
	public int getNewIdForRuta() {
		int newID = 0;
		String sql = "SELECT last_value from rutas_id_seq ";
		try {
			ResultSet resultSet = st.executeQuery(sql);			
			resultSet.next();
			newID = resultSet.getInt(1);
			newID++;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return newID;
	}

	/**
	 * 
	 * @return el ultim id de la taula documentos
	 */
	public int getNewIdForDocument() {		
		int newID = 0;
		String sql = "select last_value from documentos_id_seq";
		try {
			ResultSet resultSet = st.executeQuery(sql);
			resultSet.next();
			newID = resultSet.getInt(1);
			newID++;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return newID;
	}

	/**
	 * metodo que añade una ruta nuevas
	 * 
	 * @param nombre nombre de la ruta
	 * @param descripcion descripcion de la ruta
	 * @param desnivel el desnivel de la ruta
	 * @param nombrex nombre del fixero xml de resumen
	 * @param userId el id del usuario que añade la ruta
	 */
	public void insertRuta(String nombre, String descripcion, double desnivel,
			String nombrex, int userId) {
		String sql = "insert into rutas (nombre,descripcion,desnivel,nombrex,user_id) values(?,?,?,?,?)";
		try {
			PreparedStatement Pst = con.prepareStatement(sql);
			Pst.setString(1, nombre);
			Pst.setString(2, descripcion);			
			Pst.setDouble(3, desnivel);
			Pst.setString(4, nombrex);
			Pst.setInt(5, userId);
			Pst.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param nombre nombre del fichero
	 * @param idRuta id de la ruta relcaionado con el fichero 
	 */
	public void insertDocument(String nombre, int idRuta) {
		String sql = "insert into documentos (nombre,id_ruta) values(?,?)";
		try {
			PreparedStatement Pst = con.prepareStatement(sql);
			Pst.setString(1, nombre);			
			Pst.setInt(2, idRuta);
			Pst.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public ArrayList<File> getAllRutasFromUser(){
		return null;
	}
}
